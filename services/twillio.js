var superagent = require("superagent")

var numbers = {"lem" : "+639954891998","dada" : "+639958891748", "dadaCorphone" : "+639171581345", "dadaSmart" : "+639293591243"}

function SuperAgent(){

}

SuperAgent.sendMessage = function (message,toSend){
    superagent.post(getUrl())
        .auth(process.env.TWILLIO_TEST_SID, process.env.TWILLIO_TEST_TOKEN)
        .type("form")
        .send({"To" : numbers[toSend]})
        .send({"From" : process.env.TWILLIO_NUM})
        .send({"Body": message})
        .end(function(err,res){
            console.log(err,res.body)
        })
}

function getUrl(){
    return "https://api.twilio.com/2010-04-01/Accounts/"+process.env.TWILLIO_TEST_SID+"/Messages.json"
}

module.exports = SuperAgent