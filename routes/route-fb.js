var express = require("express")
var router = express.Router()

//services
var twillio = require("../services/twillio.js")

router.use(function(req,res,next){
    next()
})

router.post("/webhook/messaging", function(req, res){
    var message = req.body.entry[0].messaging[0].message.text.split("@")
    var recepients = []
    for (i = 1; i<message.length;i++){
        twillio.sendMessage(message[0],message[i])
    }

    res.send(req.data)
})

router.get("/webhook/messaging", function(req, res){
    res.status(200).send(req.query["hub.challenge"])
})

module.exports = router