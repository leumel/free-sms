var dotEnv = require("dotenv")
dotEnv.config()

const PORT = process.env.PORT || 5002

var express = require("express")
var app = express()
var server = require("http").Server(app)
var bodyparser = require("body-parser")

//routes
var fbRoute = require("./routes/route-fb.js")


app.use(bodyparser.urlencoded({extended: false}))
app.use(bodyparser.json())

app.use("/fb",fbRoute)

server.listen(PORT, function(){
    console.log(`started on ${PORT}`)
})


